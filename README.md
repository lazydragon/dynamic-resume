# Dynamic Resume
## A Gitlab CI/CD pipeline to render a resume built using markdown to PDF and publishing it to Gitlab pages using Pandoc.

Credits: [John Bokma](tab:https://github.com/john-bokma/resume-pandoc)

Full instructions and explanation is provided on my [blog post](https://vbh.ai/gitlab-cv/). The TLDR; is listed below.

## Instructions:
1. Fork this repo.
2. This repo contains my Resume.md as a sample. Edit the Resume.md file. The variable spec  about the YAML block are as follows:<br>
name : the name on the resume.<br>
keywords : keywords to be added to the PDF file.<br>
left-column : a list of lines you want in the left column, directly under the name on the first page.<br>
right-column : a list of lines you want in the right column, directly under the name on the first page. <br>
fontsize : default ```10pt```.<br>
fontenc : default ```T1```.br>
urlcolor : used in PDF, default ```blue```.<br>
linkcolor : used in PDF, default ```magenta```.<br>
numbersections : number sections, default off. Can also be controlled using the ```pandoc``` option ```-N, --number-sections```.<br>
name-color : the SVG name of the font color used for your name on the resume. For example ```DarkSlateGray```. Note that this option also changes the font used for your name to bold and sans serif.<br>
section-color : the SVG name of the font color used for sections. For example ```Tomato```. Note that this option also changes the section font to bold and sans serif.<br>
Regarding the last two options: if you just want to change the font to sans serif bold you can just use the color ```black```.<br>
3. Commit the changes. The build pipeline will run automatically on commit.
4. Inspect the job artifacts for the rendered Resume.pdf
5. Go to Settins > Pages to get your Gitlab pages URL and navigate to the <url>/Resume.pdf

## Example PDF:
https://lazydragon.gitlab.io/dynamic-resume/Resume.pdf

## Support
If you would like any help with the instructions, reach out to me on Matrix. My contact info is available on vbh.ai
If you would like to support me, star this repository and/or toast the blog post.
