---
name: Vasu Bhatia
keywords: python, machine learning
left-column:
 - 'Aspiring Quantitative Analyst'
 - 'E-mail: [hi@vbh.ai](mailto:hi@vbh.ai)'
 - 'Phone: +44 11111 11111'
right-column:
 - 'Location: Edinburgh, Scotland'
 - 'Personal Site: [vbh.ai](vbh.ai)'
 - 'Last Updated: \today'
...

# Summary

A creative and motivated logician with strong technical fundamentals and aptitude. Proficient in Python, machine learning and statistical analyses with an aptitude for deriving interesting insights through a data-driven approach. Excellent communication and interpersonal skills with an ever-learning and adaptive attitude. Looking to apply my knowledge to the domain of cryptocurrency and DeFi as a machine learning expert and work with a reputable cryptocurrency firm.

# Technical Skills

Python • Object Oriented Programming • SQL • Django • Bash Scripting • Artificial Intelligence • Machine Learning • Deep Learning • Data Preparation • Statistical Data Analysis • Tableau • TensorFlow

# Education

## University of St.\ Andrews - MSc.\ Artificial Intelligence

**Sep 2019 - Sep 2021**

- Achieved a distinction and was placed on the Dean's List for academic excellence
- Dissertation: A machine learning framework for recurrence prediction of clear cell renal cell carcinoma after nephrectomy; *Dissertation grade: 18.5/20*
- Core Courses: Machine Learning, Artificial Intelligence Principles, Human Computer Interaction Principles and Methods, Information Visualisation, Object-Oriented Modelling Design and Programming, Language and Computation
- Took up professional skills development courses such as social responsibility, critical and creative thinking, communication skills

## Amity University - BTech.\ Computer Science and Engineering

**Jul 2014 - May 2018**

- Major project: Machine learning based intrusion detection system in a cloud computing network; *Major project grade: 9/10*
- Core courses: Probability and Statistics, Data Structures, Object Oriented Programming, Artificial Intelligence, Java Programming, Numerical Methods and Optimization, Software Engineering
- Took up personal and professional development courses such as business communication, behavioural science and attended a military training camp
- Member of the ACM Student Chapter and involved in organising various events
- Volunteer and member of the core team for Confluence- an annual international Computer Science conference

# Experience

## University of St.\ Andrews - Research Collaborator

**Apr 2021 - Jul 2021** 

- Technical skills and tools: Machine learning, python programming, sklearn, data preparation and wrangling, statistical data analysis, data visualisation, WEKA, multiple-instance classification
- Collaborated with the Department of Medicine as part of my MSc.\ dissertation to analyse the role of histopathological data in renal cell carcinoma recurrence prediction
- Responsible for data preparation and transformation into a suitable format for modern machine learning
- Created a machine learning pipeline to evaluate the feature set and performed rigorous statistical analyses of the performance of various machine learning algorithms on the data

## Radix IT Solutions Ltd - Director

**Aug 2020 - Jan 2021 (6 months)**

- Skills used: Cloud hosting and virtualisation using KVM, customer service operations, HTML and Wordpress with WHMCS billing backend, B2B engagement
- A one person company which dealt with providing cheap hosting by setting up a KVM cloud infrastructure through Virtualizor and SolusVM on dedicated hardware leased from Wowrack

## Allena Autos Pvt Ltd - Engineer Intern

**Jan 2017 - Aug 2017 (8 months)**

- Skills used: SQL, Python programming, machine learning, predictive analyses, Tableau
- Responsible for organising and cleaning up production and sales data for statistical analyses
- Created a machine learning pipeline for predicting annual costs, requirements and profits based on historical data which resulted in a robust inventory management system and decreased annual spends

## Rotary International - Volunteer

**2010 - 2014 (5 years)**

- Part of the team responsible for installing computer hardware at schools and teaching basic IT skills to students
- Involved in volunteer work such as setting up camps for free eye check ups, free healthcare checkups
- Volunteer for Rotary's polio eradication programme


# Courses & Certifications

DeepLearning.AI TensorFlow Developer Professional Certificate
: Convolutional Neural Networks, Natural Language Processing, Sequences, Time Series and Prediction; Final Grade: 100%; Sep 2021, [Course Info](https://www.coursera.org/professional-certificates/tensorflow-in-practice); [Verified Certificate](https://www.coursera.org/account/accomplishments/professional-cert/NPBGGY5ZLV2N)

Business Analysis Foundations
: Needs Assessment, Stakeholder Involvement, Project Planning, Determining Requirements, Release Planning; Final Grade: 100%; Aug 2021; [Course Info](https://www.linkedin.com/learning/business-analysis-foundations-4); [Verified Certificate](https://www.linkedin.com/learning/certificates/46d09001833e1e4205b9e4de29b1b661e86064ca301843db4eefdc1a1435b19a?trk=share_certificate)

# Extra-curricular Activities and Personal Interests

Digital privacy and self-hosting enthusiast
: Self-hosted cloud services such as Nextcloud, [Searx Meta Search Engine](search.vbh.ai), photo management suite using docker and Photoprism and custom CalyxOS Android ROM for privacy. Huge advocate for open source software and security.

Cryptocurrency, DeFi and Trading Enthusiast
: Regularly explore different blockchains and DeFi projects and investment opportunities such as liquidity pools, staking, farming tokens, etc while looking to create algorithmic trading strategies in Python for Freqtrade

Classical guitar player and composer
: Regularly compose fingerstyle guitar tunes such as [Untangled](https://distrokid.com/hyperfollow/vasubhatia/untangled-remastered). Former member of Music Society at Amity University and a regular participant in school festivals and competitions.
